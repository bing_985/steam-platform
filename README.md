# Steam 游戏展示平台

一个模仿 Steam 的游戏展示平台，包含用户登录、游戏展示、游戏详情、评论等功能。

## 功能特性

- 用户认证
  - 登录/登出功能
  - JWT token 认证
  - 会话保持

- 游戏展示
  - 游戏列表展示
  - 游戏分类筛选
  - 价格区间筛选
  - 游戏搜索功能
  - 鼠标特效

- 游戏详情
  - 游戏详细信息展示
  - 用户评价系统
  - 评分功能

## 技术栈

- 前端
  - 原生 JavaScript
  - HTML5
  - CSS3
  - Font Awesome 图标

- 后端
  - Node.js
  - Express.js
  - MySQL
  - JWT 认证

## 项目结构
```
  steam-platform/
  ├── demo/ # 前端代码
  │ ├── html/ # HTML 文件
  │ ├── css/ # 样式文件
  │ ├── js/ # JavaScript 文件
  │ └── sql/ # SQL 脚本
  ├── server/ # 后端代码
  │ ├── app.js # 主应用文件
  │ └── config.js # 配置文件
  └── README.md # 项目说明文档
```

  
## 部署步骤

### 1. 环境准备

确保您的系统已安装：
- Node.js (v14+)
- MySQL (v5.7+)
- Git

### 2. 克隆项目
 ``` bash
  git clone https://gitee.com/bing_985/steam-platform.git
  cd steam-platform
 ```

### 3. 安装依赖
 ``` bash
      npm install
 ```

3. 创建并配置 config.js：
 ``` javascript
module.exports = {
db: {
host: 'localhost',
user: 'root',
password: 'your_password', // 修改为您的MySQL密码
database: 'demo'
},
jwt: {
secret: 'your_jwt_secret' // 修改为您的JWT密钥
},
port: 3000
};
 ```

 
### 5. 初始化用户数据

运行用户初始化脚本：

``` bash
node server/init-users.js
```

这将创建以下测试账号：
- 用户名：admin，密码：admin123
- 用户名：test，密码：test123
- 用户名：user1，密码：password123

### 6. 启动服务

1. 启动后端服务：

``` bash
node server/app.js
```

2. 访问应用：
打开浏览器访问 `http://localhost:3000/demo/html/login.html`

## 常见问题

1. 数据库连接失败
   - 检查 MySQL 服务是否启动
   - 验证数据库用户名和密码是否正确
   - 确保数据库名称为 'demo'

2. 登录失败
   - 确保已运行 init-users.js 创建测试账号
   - 检查数据库中 users 表是否有数据

3. 游戏列表显示失败
   - 确保已导入 games.sql
   - 检查浏览器控制台是否有错误信息

## 开发团队

- 前端开发：[bing_985](https://gitee.com/bing_985)
- 后端开发：[bing_985](https://gitee.com/bing_985)
- UI 设计：[AI](https://gitee.com/bing_985)

## 许可证
本项目使用 MIT 许可证。
MIT License

## 联系方式

如有问题，请联系：[1841248198@qq.com](1841248198@qq.com)

## 项目截图

![alt text](image.png)

![alt text](img2.png)