CREATE TABLE IF NOT EXISTS games (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    image_url VARCHAR(255) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    genres JSON,
    tags JSON,
    rating DECIMAL(3, 1),
    description TEXT,
    release_date DATE,
    developer VARCHAR(100),
    publisher VARCHAR(100),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- 插入示例数据
INSERT INTO games (title, image_url, price, genres, tags, rating, description, developer, publisher) VALUES
(
    '赛博朋克2077',
    'https://cdn.akamai.steamstatic.com/steam/apps/1091500/header.jpg',
    298.00,
    '["action", "rpg"]',
    '["开放世界", "科幻", "角色扮演"]',
    4.5,
    '在赛博朋克2077中，您将扮演一位野心勃勃的雇佣兵：V，追寻一种独特的植入体——获得永生的关键。',
    'CD PROJEKT RED',
    'CD PROJEKT RED'
),
(
    '艾尔登法环',
    'https://cdn.akamai.steamstatic.com/steam/apps/1245620/header.jpg',
    398.00,
    '["action", "rpg"]',
    '["魂类", "开放世界", "黑暗奇幻"]',
    4.8,
    '艾尔登法环是一款动作角色扮演游戏，由宫崎英高指导，乔治·R·R·马丁参与创作世界观。',
    'FromSoftware',
    'BANDAI NAMCO'
),
(
    '荒野大镖客：救赎2',
    'https://cdn.akamai.steamstatic.com/steam/apps/1174180/header.jpg',
    249.00,
    '["action", "adventure"]',
    '["开放世界", "西部", "剧情"]',
    4.9,
    '亚瑟·摩根和范德林德帮众是一群亡命之徒，联邦侦探和最优秀的赏金猎人紧追不舍。',
    'Rockstar Games',
    'Rockstar Games'
),
(
    '战神：诸神黄昏',
    'https://cdn.akamai.steamstatic.com/steam/apps/1593500/header.jpg',
    398.00,
    '["action", "adventure"]',
    '["动作冒险", "北欧神话", "剧情"]',
    4.9,
    '加入奎托斯与阿特柔斯的旅程，探索北欧九界中的神秘土地，面对强大的北欧诸神与怪物。',
    'Santa Monica Studio',
    'PlayStation PC LLC'
),
(
    '文明VI',
    'https://cdn.akamai.steamstatic.com/steam/apps/289070/header.jpg',
    199.00,
    '["strategy", "simulation"]',
    '["策略", "回合制", "历史"]',
    4.7,
    '文明VI是一款回合制策略游戏，让玩家建立一个能够经受时间考验的文明。',
    'Firaxis Games',
    '2K Games'
),
(
    '星空',
    'https://cdn.akamai.steamstatic.com/steam/apps/1716740/header.jpg',
    498.00,
    '["rpg", "adventure"]',
    '["太空", "开放世界", "科幻"]',
    4.4,
    '星空是一款新一代科幻角色扮演游戏，让玩家在浩瀚的宇宙中探索、冒险。',
    'Bethesda Game Studios',
    'Bethesda Softworks'
),
(
    '死亡搁浅',
    'https://cdn.akamai.steamstatic.com/steam/apps/1190460/header.jpg',
    298.00,
    '["action", "adventure"]',
    '["开放世界", "剧情", "科幻"]',
    4.6,
    '由小岛秀夫打造的动作冒险游戏，在一个被神秘事件改变的世界中，玩家需要重新连接这个分裂的社会。',
    'KOJIMA PRODUCTIONS',
    '505 Games'
); 


INSERT INTO games (title, image_url, price, genres, tags, rating, description, developer, publisher) VALUES
(
'巫师3：狂猎',
'https://cdn.akamai.steamstatic.com/steam/apps/292030/header.jpg',
127.00,
'["rpg", "action"]',
'["开放世界", "奇幻", "剧情"]',
4.9,
'在这款角色扮演游戏中扮演传奇猎魔人杰洛特，在庞大的开放世界中展开冒险。',
'CD PROJEKT RED',
'CD PROJEKT RED'
),
(
'只狼：影逝二度',
'https://cdn.akamai.steamstatic.com/steam/apps/814380/header.jpg',
268.00,
'["action", "adventure"]',
'["魂类", "武士", "挑战"]',
4.8,
'在这款动作冒险游戏中，成为"独臂之狼"，体验宫崎英高标志性的致命战斗体验。',
'FromSoftware',
'Activision'
),
(
'底特律：化身为人',
'https://cdn.akamai.steamstatic.com/steam/apps/1222140/header.jpg',
118.00,
'["adventure"]',
'["剧情", "科幻", "选择"]',
4.7,
'在这款互动式科幻剧情游戏中，探索一个仿生人与人类共存的未来世界。',
'Quantic Dream',
'Quantic Dream'
),
(
'极限竞速：地平线5',
'https://cdn.akamai.steamstatic.com/steam/apps/1551360/header.jpg',
389.00,
'["simulation", "action"]',
'["竞速", "开放世界", "多人"]',
4.6,
'探索充满活力的开放世界墨西哥风景，体验无尽的驾驶冒险。',
'Playground Games',
'Xbox Game Studios'
),
(
'生化危机4重制版',
'https://cdn.akamai.steamstatic.com/steam/apps/2050650/header.jpg',
396.00,
'["action", "adventure"]',
'["恐怖", "生存", "动作"]',
4.8,
'经典恐怖生存游戏的现代重制版，重新体验里昂·肯尼迪的救援任务。',
'CAPCOM',
'CAPCOM'
),
(
'双人成行',
'https://cdn.akamai.steamstatic.com/steam/apps/1426210/header.jpg',
198.00,
'["adventure", "action"]',
'["合作", "解谜", "平台"]',
4.9,
'一款纯双人合作冒险游戏，玩家需要共同合作才能完成充满创意的关卡。',
'Hazelight',
'Electronic Arts'
),
(
'哈利波特：霍格沃茨之遗',
'https://cdn.akamai.steamstatic.com/steam/apps/990080/header.jpg',
298.00,
'["rpg", "action"]',
'["魔法", "开放世界", "奇幻"]',
4.5,
'在这款开放世界动作RPG中，成为霍格沃茨魔法学校的学生，创造自己的魔法冒险。',
'Avalanche Software',
'Warner Bros. Games'
),
(
'幽灵线：东京',
'https://cdn.akamai.steamstatic.com/steam/apps/1475810/header.jpg',
298.00,
'["action", "adventure"]',
'["动作", "超自然", "日本"]',
4.4,
'在一个充满超自然现象的东京中，运用灵力对抗神秘的威胁。',
'Tango Gameworks',
'Bethesda Softworks'
),
(
'十三机兵防卫圈',
'https://cdn.akamai.steamstatic.com/steam/apps/1593500/header.jpg',
279.00,
'["strategy", "rpg"]',
'["科幻", "剧情", "机甲"]',
4.8,
'横跨多个时空的复杂剧情，结合独特的机甲战斗系统的科幻冒险游戏。',
'Vanillaware',
'ATLUS'
),
(
'女神异闻录5',
'https://cdn.akamai.steamstatic.com/steam/apps/1687950/header.jpg',
329.00,
'["rpg"]',
'["JRPG", "回合制", "动漫"]',
4.9,
'在这款风格化的JRPG中，扮演怪盗团成员，在现实与认知世界中展开冒险。',
'ATLUS',
'SEGA'
);